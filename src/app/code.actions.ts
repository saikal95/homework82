import {createAction, props} from "@ngrx/store";

export const codeInsert = createAction('[doorCode] Add character', props<{char: string}>());


export const codeCheck = createAction('[doorCode] Check character');

export const deleteCode = createAction('[doorCode] Delete character');
