import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import {doorReducer} from "./code.reducer";
import { DoorcodeComponent } from './doorcode/doorcode.component';

@NgModule({
  declarations: [
    AppComponent,
    DoorcodeComponent
  ],
  imports: [
    BrowserModule,
    StoreModule.forRoot({doorCode: doorReducer}, {})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
