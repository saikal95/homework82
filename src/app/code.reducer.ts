import {createReducer, on} from "@ngrx/store";
import {codeCheck, codeInsert, deleteCode} from "./code.actions";

const correctPassword = '1337';

const initialState = {
  isCorrect: false,
  password:'',
};

export const doorReducer = createReducer(
  initialState,
  on(codeInsert, (state, {char}) => {
    let copyState = {...state, password: state.password + char};
    if (copyState.password.length === 5){
      return {...state}
    }
    return copyState;
  }),


  on(codeCheck, (state) => {
    if (correctPassword === state.password){
      return {...state, isCorrect: true}
    }
    return {...state, isCorrect: false}
  }),

  on(deleteCode, (state) => {
    let arrPassword = state.password.split('');
    let password = arrPassword.slice(1, arrPassword.length).join('')
    return {...state, password: password.toString()};
  })
)
