import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subscription} from "rxjs";
import {Store} from "@ngrx/store";
import {codeCheck, codeInsert, deleteCode} from "../code.actions";
import {CoderModel} from "../models/coder.model";

@Component({
  selector: 'app-doorcode',
  templateUrl: './doorcode.component.html',
  styleUrls: ['./doorcode.component.css']
})
export class DoorcodeComponent implements OnDestroy{
  arrNumbers = [7,8,9,4,5,6,1,2,3,0];
  doorCode! : Observable<object>;
  coderObject: CoderModel | null = null;
  arrSymbols: string[] = [];
  coderSubscription!: Subscription;
  icCLicked = false;

  constructor(private store: Store<{doorCode: object}>) {
    this.doorCode = store.select("doorCode");
    this.coderSubscription = this.doorCode.subscribe(obj => {
      this.coderObject = <CoderModel>obj;
      this.arrSymbols = this.coderObject.password.split('');
    });
  }

  codeInsert(num: number){
    this.store.dispatch(codeInsert({char: num.toString()}));

  }

  ngOnDestroy() {
    this.coderSubscription.unsubscribe();
  }

  checkCode() {
    this.store.dispatch(codeCheck());
  }

  onRemove() {
    this.store.dispatch(deleteCode());

  }
}
